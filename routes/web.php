<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    /** --------------------------------------- Validacion de nombres ----------------------------------------- */
    Route::get('validacion/nombres', [
        'uses' => 'Validacion\ValidacionController@index',
        'as' => 'validacion.nombres.index'
    ]);

    Route::post('validacion/nombres/buscar', [
        'uses' => 'Validacion\ValidacionController@buscar',
        'as' => 'validacion.nombres.buscar'
    ]);

    Route::get('validacion/nombres/exportar/{nombre}/{porcentaje}/excel', [
        'uses' => 'Validacion\ValidacionController@exportarExcel',
        'as' => 'validacion.nombres.exportar.excel'
    ]);

    Route::get('validacion/nombres/exportar/{nombre}/{porcentaje}/pdf', [
        'uses' => 'Validacion\ValidacionController@exportarPDF',
        'as' => 'validacion.nombres.exportar.pdf'
    ]);
    /** --------------------------------------- Validacion de nombres ----------------------------------------- */

});

