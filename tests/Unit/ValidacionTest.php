<?php

namespace Tests\Unit;

use App\Exports\validacion\NombresExport;
use App\src\validacion\PersonaPublica;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;


class ValidacionTest extends TestCase
{
    public $personaPublica;
    use WithFaker;

    /**
     * Setup tests
     */
    public function setUp ():void
    {
        parent::setUp();


        $this->setUpFaker();

        $this->personaPublica = factory(PersonaPublica::class)->create();
    }

    /**
     * Test main page validation index
     * @test
     */
    function it_loads_validacion_index()
    {
        $this->get('/validacion/nombres')->assertStatus(302);
    }

    /**
     * Test search
     * @test
     */
    function it_loads_validacion_buscar()
    {
        $this->withoutMiddleware();

        $personasPublicas = factory(PersonaPublica::class,100)->create();
        $nombre = $this->faker->name;
        $porcentaje = $this->faker->randomNumber(2);

        //Datos de la comparación
        $datos = [
            'personasPublicas' => $personasPublicas,
            'nombre' => $nombre,
            'porcentaje' => $porcentaje,
        ];

        $this->post('/validacion/nombres/buscar',$datos)->assertStatus(200);
    }

    /**
     * Test user can download excel
     * @test
     */
    function user_can_download_excel_export()
    {
        $nombre = $this->faker->name;
        $porcentaje = $this->faker->randomNumber(2);

        $this->get('/validacion/nombres/exportar/'.$nombre.'/'.$porcentaje.'/excel')->assertStatus(302);
    }

    /**
     * Test user can download pdf
     * @test
     */
    function user_can_download_pdf_export()
    {
        $nombre = $this->faker->name;
        $porcentaje = $this->faker->randomNumber(2);

        $this->get('/validacion/nombres/exportar/'.$nombre.'/'.$porcentaje.'/pdf')->assertStatus(302);
    }
}
