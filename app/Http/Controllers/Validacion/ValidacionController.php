<?php

namespace App\Http\Controllers\Validacion;

use App\Exports\validacion\NombresExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\validacion\nombres\ValidacionNombresRequest;
use App\src\validacion\PersonaPublica;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;

class ValidacionController extends Controller
{
    /**
     * Show the form to validate.
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('validacion.nombres.index');
    }

    /**
     * Find the matches function
     * @param ValidacionNombresRequest $request
     * @return mixed
     */
    public function buscar(ValidacionNombresRequest $request)
    {
        // recibimos los parámetros de busqueda para volverlos a enviar a la vista
        $nombre = strtolower ($request->nombre);
        $porcentaje = $request->porcentaje;

        // Gereneramos el arreglo de coincidencias
        $arrayPersonasResultados = $this->coincidenciaPersonasPorcentaje($nombre,$porcentaje);

        return view('validacion.nombres.resultado')
            ->with('nombre',$nombre)
            ->with('porcentaje',$porcentaje)
            ->with('arrayPersonasResultados',$arrayPersonasResultados);
    }

    /**
     * Find the matches function
     * @param ValidacionNombresRequest $request
     * @return array $arrayPersonasResultados
     */
    public function coincidenciaPersonasPorcentaje($nombre,$porcentaje)
    {
        //normalizado del nombre buscado
        $nombreBuscadoComun = $this->normalizarNombre($nombre);

        //obtenemos registros de la base de datos
        $personasPublicas = PersonaPublica::all();

        $arrayPersonasResultados = [];

        //recorremos los datos de las base de datos para comparar similitudes y agregar a un arreglo del resultado
        foreach ($personasPublicas as $personaPublica)
        {
            $nombreBaseDatos = strtolower ($personaPublica->nombre);
            $nombreBaseDatosComun = $this->normalizarNombre($nombreBaseDatos);

            $porcentajeSimilitud = $this->comparacionNombres($nombreBuscadoComun,$nombreBaseDatosComun);

            //comparamos si el porcentaje de similitud es superior o igual al deseado para agregar esta persona a la lista de resultados
            if($porcentajeSimilitud>=$porcentaje)
            {
                $personaPublica->append('porcentaje');
                $personaPublica->porcentaje = $porcentajeSimilitud;
                array_push($arrayPersonasResultados, $personaPublica);
            }
        }

        return $arrayPersonasResultados;
    }

    /**
     * Replace letters that sound similar
     * @param string $nombre
     * @return string $nombreComun4
     */
    public function normalizarNombre($nombre)
    {
        /**
         * Reemplaza las letras que tienen sonido similar
         * Equivalencias:
         * s=z
         * c=k
         * b=v
         * i=y
         */

        $nombreComun1=str_ireplace("z","s",$nombre);
        $nombreComun2=str_ireplace("k","c",$nombreComun1);
        $nombreComun3=str_ireplace("v","b",$nombreComun2);
        $nombreComun4=str_ireplace("y","i",$nombreComun3);

        return $nombreComun4;
    }

    /**
     * Compare the similarity of words
     * @param string $nombre1 el nombre buscado
     * @param string $nombre2 el nombre en la base de datos
     * @return int $porcentaje
     */
    public function comparacionNombres($nombre1,$nombre2)
    {
        //partimos la cadena en cada palabra
        $nombres1 = explode(" ", $nombre1);
        $nombres2 = explode(" ", $nombre2);

        $totalParecido = 0; // para hacer una sumatoria del porcentaje de similitud entre las palabras y luego dividir por cantidad de palabras

        //validamos para saber cuál fue la cantidad máxima contra la que se comparó para dividir al final por la cantidad de recorrido secundario
        if(count($nombres1)>count($nombres2))
        {
            $totalRecorrido=count($nombres1);
        }
        else{
            $totalRecorrido=count($nombres2);
        }
        //$totalRecorrido = count($nombres2); // para dividir al final por la cantidad de recorrido secundario

        $maximoParecido = 0; // variable para obtener el máximo parecido en un ciclo
        // recorremos el arreglo del nombre buscado
        foreach ($nombres1 as $nombre1x)
        {
            // variable para saber la posicion donde se encontró mayor coincidencia entre palabras
            $posicion = -1;
            // recorremos el arreglo del nombre en la base de datos para ver en cuanto máximo se parece la palabra 1 a las palabras 2
            foreach ($nombres2 as $key => $nombre2x)
            {
                // aplicamos la funcion similar text para validar la similitud de las palabras
                similar_text($nombre1x,$nombre2x,$percent);
                // validamos para actualizar el mayor porcentaje de coincidencia de la palabra 1 con las palabras 2
                if($percent>=$maximoParecido)
                {
                    $maximoParecido = $percent;
                    $posicion = $key;
                }
            }
            // sumamos el mayor valor obtenido de coincidencia de la palabra 1 con cualquier palabra 2
            $totalParecido = $totalParecido + $maximoParecido;
            // eliminamos del arreglo la posicion encontrada con mayor coincidencia para no volver a compararla
            unset($nombres2[$posicion]);
            // reiniciamos la variable de máximo parecido de una palabra
            $maximoParecido = 0;
        }
        // se divide el total obtenido en porcentaje de similitudes en la cantidad palabras que tiene el campo de la base de datos
        $porcentaje = round($totalParecido/$totalRecorrido,2);

        return $porcentaje;
    }

    /**
     * Export result to excel
     * @param string $nombre
     * @param int $porcentaje
     * @return excel Excel
     */
    public function exportarExcel($nombre,$porcentaje)
    {
        // Gereneramos el arreglo de coincidencias
        $arrayPersonasResultados = $this->coincidenciaPersonasPorcentaje($nombre,$porcentaje);

        return Excel::download(new NombresExport($nombre, $porcentaje, $arrayPersonasResultados), ' Resultado_validacion_de_' . $nombre . '_con_' . $porcentaje . '_porcentaje.xlsx');

    }

    /**
     * Export result to PDF
     * @param string $nombre
     * @param int $porcentaje
     * @return excel Excel
     */
    public function exportarPDF($nombre,$porcentaje)
    {
        // Gereneramos el arreglo de coincidencias
        $arrayPersonasResultados = $this->coincidenciaPersonasPorcentaje($nombre,$porcentaje);

        // Creamos el archivo pdf
        $pdf = App::make('dompdf.wrapper');
        // Cargamos la vista que contiene la estructura del pdf y enviamos los datos que necesitamos mostrar
        $pdf->loadView('validacion.nombres.exports.resultadoPDF', ['nombre' => $nombre, 'porcentaje' => $porcentaje,'arrayPersonasResultados' => $arrayPersonasResultados]);
        // Retornamos el achivo pdf creado y damos el nombre con el que se va a descargar
        return $pdf->download('Resultado_validacion_de_' . $nombre . '_con_' . $porcentaje . '_porcentaje.pdf');
        // Usar STREAM en lugar de DOWNLOAD si no se desea descagar directamente el PDF

    }
}
