<?php

namespace App\src\validacion;

use Illuminate\Database\Eloquent\Model;

class PersonaPublica extends Model
{
    protected $table = 'personas_publicas';
    protected $fillable = ['departamento', 'localidad', 'municipio', 'nombre', 'anios_activo', 'tipo_persona', 'tipo_cargo'];
}
