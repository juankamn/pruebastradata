<?php

namespace App\Exports\validacion;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class NombresExport implements FromView, ShouldAutoSize, WithEvents
{
    private $nombre;
    private $porcentaje;
    private $arrayPersonasResultados;

    public function __construct($nombre,$porcentaje,$arrayPersonasResultados)
    {
        $this->nombre = $nombre;
        $this->porcentaje = $porcentaje;
        $this->arrayPersonasResultados = $arrayPersonasResultados;
    }

    public function view(): View
    {

        return view('validacion.nombres.exports.resultadoExcel', [
            'nombre' => $this->nombre,
            'porcentaje' => $this->porcentaje,
            'arrayPersonasResultados' => $this->arrayPersonasResultados,
        ]);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        $maximo = count($this->arrayPersonasResultados) + 4; // número máximo de filas

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];

        return [
//            BeforeExport::class  => function(BeforeExport $event) {
//                $event->writer->setCreator('Juan Camilo Macias Navarrete');
//            },
            AfterSheet::class    => function(AfterSheet $event) use ($styleArray,$maximo) {

                //Se asigna el borde
                $event->sheet->getStyle('A1:F'.$maximo)->applyFromArray($styleArray);
                $event->sheet->getPageSetup()->setPrintArea('A1:F'.$maximo);

                // Para centrar el encabezado
                $event->sheet->getStyle('A1:C2')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getStyle('A1:C2')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

                $event->sheet->getStyle('A4:F4')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getStyle('A4:F4')->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);


            },
        ];
    }
}
