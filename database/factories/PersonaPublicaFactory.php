<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\src\validacion\PersonaPublica::class, function (Faker $faker) {
    return [
        'departamento' => $faker->city,
        'localidad' => $faker->city,
        'municipio' => $faker->city,
        'nombre' => $faker->name,
        'anios_activo' => $faker->randomNumber(2),
        'tipo_persona' => $faker->domainName,
        'tipo_cargo' => $faker->domainWord,
    ];
});
