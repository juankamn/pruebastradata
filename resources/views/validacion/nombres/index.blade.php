@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Validación de nombres') }}</div>

                    <div class="card-body">

                        <div class="form-group">
                            <form method="POST" action="{{ route('validacion.nombres.buscar') }}">
                                @csrf

                                <table class="table table-bordered">
                                    <tr>
                                        <th class="text-center">Nombre a validar</th>
                                        <th class="text-center">Porcentaje de coincidencia</th>
                                    </tr>
                                    <tr>
                                        <td class="text-center"><input type="text" id="nombre" name="nombre" required
                                                                       minlength="4" maxlength="255" size="20"></td>
                                        <td class="text-center"><input type="number" id="porcentaje" name="porcentaje"
                                                                       required min="0" max="100">%
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" colspan="2">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Validar') }}
                                                </button>
                                        </td>
                                    </tr>
                                </table>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
