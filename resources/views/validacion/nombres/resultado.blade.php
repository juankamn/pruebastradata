@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{route('validacion.nombres.index')}}" class="btn btn-link">
                Volver
            </a>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Validación de nombres') }}</div>

                    <div class="card-body">

                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center">Nombre a validar</th>
                                <th class="text-center">Porcentaje de coincidencia buscado</th>
                            </tr>
                            <tr>
                                <td class="text-center">{{$nombre}}</td>
                                <td class="text-center">{{$porcentaje}} %</td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div
                        class="card-header">{{ __('Resultados obtenidos:') }} {{ count($arrayPersonasResultados) }}</div>

                    <div class="card-body">
                        <div class="text-right">
                            <div>
                                <a href="{{route('validacion.nombres.exportar.excel',[$nombre,$porcentaje])}}"
                                   title="Excel"
                                   class="btn btn-success">Excel</a>
                            </div>
                            <div>
                                <a href="{{route('validacion.nombres.exportar.pdf',[$nombre,$porcentaje])}}"
                                   title="PDF"
                                   class="btn btn-secondary">PDF</a>
                            </div>
                        </div>

                        <table class="table table-bordered">
                            <tr>
                                <th class="text-center">Porcentaje de coincidencia</th>
                                <th class="text-center">Nombre</th>
                                <th class="text-center">Tipo Persona</th>
                                <th class="text-center">Tipo Cargo</th>
                                <th class="text-center">Años activo</th>
                                <th class="text-center">Departamento</th>
                            </tr>
                            @foreach($arrayPersonasResultados as $persona)
                                <tr>
                                    <td>{{$persona->porcentaje}}</td>
                                    <td>{{$persona->nombre}}</td>
                                    <td>{{$persona->tipo_persona}}</td>
                                    <td>{{$persona->tipo_cargo}}</td>
                                    <td>{{$persona->anios_activo}}</td>
                                    <td>{{$persona->departamento}}</td>
                                </tr>
                            @endforeach
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
