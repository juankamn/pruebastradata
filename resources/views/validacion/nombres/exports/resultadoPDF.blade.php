<table border="1">
    <tr>
        <th class="text-center">Nombre a validar</th>
        <th class="text-center">Porcentaje de coincidencia buscado</th>
        <th class="text-center">Cantidad de coincidencias encontradas</th>
    </tr>
    <tr>
        <td class="text-center">{{$nombre}}</td>
        <td class="text-center">{{$porcentaje}} %</td>
        <td class="text-center">{{count($arrayPersonasResultados)}}</td>
    </tr>
</table>

@if(count($arrayPersonasResultados)>0)

    <table border="1">
        <tr>
            <th class="text-center">Porcentaje de coincidencia</th>
            <th class="text-center">Nombre</th>
            <th class="text-center">Tipo Persona</th>
            <th class="text-center">Tipo Cargo</th>
            <th class="text-center">Años activo</th>
            <th class="text-center">Departamento</th>
        </tr>
        @foreach($arrayPersonasResultados as $persona)
            <tr>
                <td>{{$persona->porcentaje}}</td>
                <td>{{$persona->nombre}}</td>
                <td>{{$persona->tipo_persona}}</td>
                <td>{{$persona->tipo_cargo}}</td>
                <td>{{$persona->anios_activo}}</td>
                <td>{{$persona->departamento}}</td>
            </tr>
        @endforeach
    </table>


@endif
